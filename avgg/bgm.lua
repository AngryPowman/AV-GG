
function lynne_audio_bgm(name)
    if g_lynne_play_bgm ~= nil then
        g_lynne_play_bgm:stop()
        g_lynne_play_bgm = nil
    end
    if name == "我的滑板鞋" then
        g_lynne_play_bgm = love.audio.newSource("audio/我的滑板鞋.mp3")
    elseif name == "平凡之路" then
        g_lynne_play_bgm = love.audio.newSource("audio/平凡之路.mp3")
    elseif name == "后会无期" then
        g_lynne_play_bgm = love.audio.newSource("audio/后会无期.mp3")
    end

    if g_lynne_play_bgm ~= nil then
        g_lynne_play_bgm:setVolume(0.5)
        g_lynne_play_bgm:setLooping(true)
        g_lynne_play_bgm:play()
    end
end