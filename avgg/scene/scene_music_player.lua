
local function init()
    lynne_audio_bgm("我的滑板鞋")
end

local function draw()
    love.graphics.setColor(255, 255, 0, 255)
    love.graphics.print("歌单：", 200, 100)
    love.graphics.reset()
    love.graphics.print("1：后会无期", 200, 150)
    love.graphics.print("2：平凡之路", 200, 200)
    love.graphics.print("3：我的滑板鞋", 200, 250)
    love.graphics.print("Press 'Enter' to return.", 100, 500)
end

local function keypressed(key)
    if key == '1' then
        lynne_audio_bgm("后会无期")
    elseif key == '2' then
        lynne_audio_bgm("平凡之路")
    elseif key == '3' then
        lynne_audio_bgm("我的滑板鞋")
    elseif key == 'return' then
        local s = require('engine/scene')
        s.retreat()
    end
end

local x = {}
x.init = init
x.draw = draw
x.keypressed = keypressed
return x
