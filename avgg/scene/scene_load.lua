
local scene_list = {}

scene_list['welcome']       = require 'scene/scene_welcome'
scene_list['main_menu']     = require 'scene/scene_main_menu'
scene_list['about']         = require 'scene/scene_about'
scene_list['music_player']  = require 'scene/scene_music_player'

return scene_list
