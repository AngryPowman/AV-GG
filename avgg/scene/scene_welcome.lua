
local function draw()
    love.graphics.setColor(255, 255, 0, 255)
    love.graphics.print("Welcome!", 200, 100)
    love.graphics.reset()
    love.graphics.print("Press 'Enter' to continue.", 200, 150)
end

local function keypressed(key)
    if key == 'return' then
        local s = require('engine/scene')
        s.switch('main_menu')
    end
end

local x = {}
x.draw = draw
x.keypressed = keypressed
return x
