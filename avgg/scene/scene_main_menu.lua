local selection = 1

local menu_list = {
    '开始',
    '音乐',
    '关于',
    '退出'
}

local function draw()
    for i = 1,4 do
        if i == selection then
            love.graphics.printf("- "..menu_list[i].." -", 0, 150+i*40, 800, "center")
        else
            love.graphics.printf(menu_list[i], 0, 150+i*40, 800, "center")
        end
    end
end

local function menu_click()
    if selection==1 then
        local s = require('engine/scene')
        s.switch('music_player')
    elseif selection==2 then
        local s = require('engine/scene')
        s.switch('music_player')
    elseif selection==3 then
        local s = require('engine/scene')
        s.switch('about')
    elseif selection==4 then
        love.event.quit()
    end
end

local function keypressed(key)
    if key == 'up' and selection>1 then
        selection = selection-1
    elseif key == 'down' and selection<4 then
        selection = selection+1
    elseif key == 'return' then
        menu_click()
    end
end

local x = {}
x.draw = draw
x.keypressed = keypressed
return x
