
local scene_list = require 'scene/scene_load'
local scene_current = scene_list['welcome']

local function lynne_engine_scene_init()
    for _,x in pairs(scene_list) do
        if x.init ~= nil then
            x.init()
        end
    end
end

local function lynne_engine_scene_update(dt)
    if scene_current ~= nil then
        if scene_current.update ~= nil then
            scene_current.update(dt)
        end
    end
end
local function lynne_engine_scene_draw()
    if scene_current ~= nil then
        if scene_current.draw ~= nil then
            scene_current.draw()
        end
    end
end
local function lynne_engine_scene_mousepressed(x, y, button)
    if scene_current ~= nil then
        if scene_current.mousepressed ~= nil then
            scene_current.mousepressed(x, y, button)
        end
    end
end
local function lynne_engine_scene_mousereleased(x, y, button)
    if scene_current ~= nil then
        if scene_current.mousereleased ~= nil then
            scene_current.mousereleased(x, y, button)
        end
    end
end
local function lynne_engine_scene_keypressed(key)
    if scene_current ~= nil then
        if scene_current.keypressed ~= nil then
            scene_current.keypressed(key)
        end
    end
end
local function lynne_engine_scene_keyreleased(key)
    if scene_current ~= nil then
        if scene_current.keyreleased ~= nil then
            scene_current.keyreleased(key)
        end
    end
end

--先不搞栈，只支持回退两次
local retreat1 = scene_current
local retreat2 = scene_current
local function lynne_engine_scene_switch(name)
    retreat2 = retreat1
    retreat1 = scene_current
    scene_current = scene_list[name]
end
local function lynne_engine_scene_retreat()
    scene_current = retreat1
    retreat1 = retreat2
end

local scene = {}
scene.init          = lynne_engine_scene_init
scene.update        = lynne_engine_scene_update
scene.draw          = lynne_engine_scene_draw
scene.mousepressed  = lynne_engine_scene_mousepressed
scene.mousereleased = lynne_engine_scene_mousereleased
scene.keypressed    = lynne_engine_scene_keypressed
scene.keyreleased   = lynne_engine_scene_keyreleased
scene.switch        = lynne_engine_scene_switch
scene.retreat       = lynne_engine_scene_retreat
return scene
