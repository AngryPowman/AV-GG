local bgm = nil

local bgm_list = {
    ["我的滑板鞋"] = love.audio.newSource("data/bgm/我的滑板鞋.mp3"),
    ["平凡之路"] = love.audio.newSource("data/bgm/平凡之路.mp3"),
    ["后会无期"] = love.audio.newSource("data/bgm/后会无期.mp3")
}

function lynne_audio_bgm(name)
    if bgm ~= nil then
        bgm:stop()
        bgm = nil
    end

    bgm = bgm_list[name]

    if bgm ~= nil then
        bgm:setVolume(0.5)
        bgm:setLooping(true)
        bgm:play()
    end
end
