require 'graphics/info'
require 'graphics/tiles'
require 'graphics/map'

function lynne_graphics_init()
    lynne_graphics_tiles_load()
    lynne_graphics_map_load_tiles()
    lynne_graphics_map_load_maps()
end

function lynne_graphics_draw()
    lynne_graphics_map_draw()
    lynne_graphics_info_display()
end

function lynne_graphics_update(dt)
    local speed = 100 * dt
    lynne_graphics_map_update(speed)
end