local cg_image_table = {}

function lynne_cg_init()
        cg_image_table[1] = love.graphics.newImage("data/cg/魔法阵1.png")
        cg_image_table[2] = love.graphics.newImage("data/cg/魔法阵2.png")
end

function lynne_cg_load(name)
    if name == nil then
        cg_lynne_image = nil
    elseif name == "魔法阵1" then
        cg_lynne_image = cg_image_table[1]
    elseif name == "魔法阵2" then
        cg_lynne_image = cg_image_table[2]
    end
end

function  lynne_cg_draw()
    if cg_lynne_image ~= nil then     
        love.graphics.draw(cg_lynne_image)
    end
end