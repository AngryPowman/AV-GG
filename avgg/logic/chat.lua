


local g_chat_sample = {
    {"卡拉","你好"},
    {"卡拉","呵呵"},
    {"泡面","我去洗澡"}
}

local chat_avatar = {}

chat_avatar["泡面"] = love.graphics.newImage("data/chat_avatar/powman.png")
chat_avatar["卡拉"] = love.graphics.newImage("data/chat_avatar/sum.png")

local curChat = nil

function lynne_logic_chat_get(name)
    if name == "a" then
        return g_chat_sample
    end
    return nil
end

function logic_chat_draw()
    if curChat ~= nil then
        love.graphics.draw(chat_dialog_img, 0, 360)
        local avatar = chat_avatar[curChat[1]]
        if avatar ~= nil then
            local width = avatar:getWidth()
            local height = avatar:getHeight()
            love.graphics.draw(avatar, 100 - width / 2, 500 - height / 2)
        end
        love.graphics.print(curChat[1], 230, 365)
        love.graphics.print(curChat[2], 230, 410)
    end
end

function lynne_logic_chat_play(chat)
    curChat = g_chat_sample[chat]
end
